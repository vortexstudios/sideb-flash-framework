﻿package com.vortexgamestudios.framework.soundmanager
{
	import appkit.responders.NResponder;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;

	public class SoundClip extends Sprite
	{
		private var _name:String;
		private var _sound:Sound;
		private var _channel:SoundChannel = new SoundChannel();
		private var _multiSound:Boolean = true;
		
		private var _position:Number = 0.0;
		private var _volume:Number = 1.0;
		
		private var _loop:Boolean = false;
		private var _loopAt:Number = 0.0;

		private var _playFrom:Number = 0.0;
		
		private var _channels:Vector.<SoundChannel> = new Vector.<SoundChannel>();

		public override function get name():String
		{
			return _name;
		}
		
		public function get sound():Sound
		{
			return _sound;
		}
		
		public function get channel():SoundChannel
		{
			return _channel;
		}
		
		public function get position():Number
		{
			return _position;
		}
		
		public function get volume():Number
		{
			return _volume;
		}
		
		public function set volume( val:Number ):void
		{
			_volume = val;
			
			for( var q = 0; q < _channels.length; q++ )
			{
				_channels[q].soundTransform = new SoundTransform( val );
			}
		}
		
		public function SoundClip( name:String, sound:Sound, loop:Boolean = false, loopAt:Number = 0.0 ):void
		{
			_name = name;
			_sound = sound;
			_loop = loop;
			_loopAt = loopAt;
		}
		
		public function play():void
		{
			if( _multiSound == false && _channels.length < 0 )
				return;
				
			var id:uint = _channels.push( new SoundChannel() ) - 1;
			
			_channels[id] = _sound.play(_playFrom);
			_channels[id].soundTransform = new SoundTransform(_volume);
			
			NResponder.addNative( _channels[id],  Event.SOUND_COMPLETE, onSoundComplete );
			
			//_channels[id].addEventListener( Event.SOUND_COMPLETE, onSoundComplete );
			//this.addEventListener( Event.ENTER_FRAME, onSoundUpdate );
		}
		
		public function stop():void
		{
			for( var q:int = 0; q < _channels.length; q++ )
			{
				_channels[q].stop();
				
				//NResponder. .addNative( _channels[id],  Event.SOUND_COMPLETE, onSoundComplete );
				//_channels[q].removeEventListener( Event.SOUND_COMPLETE, onSoundComplete );
			}
			//this.removeEventListener( Event.ENTER_FRAME, onSoundUpdate );
			
			_playFrom = 0.0;
			_position = 0.0;
		}
		
		private function onSoundComplete( e:Event ):void
		{
			//_channels.shift().removeEventListener( Event.SOUND_COMPLETE, onSoundComplete );
			/*
			(e.target as SoundChannel).removeEventListener( Event.SOUND_COMPLETE, onSoundComplete );
			
			for( var q:int = 0; q < _channels.length; q++ )
			{
				if( _channels[q] == e.target )
				{
					_channels.splice(q, 0);// .slice( q, 1 );
				}
			}
			*/

			//this.removeEventListener( Event.ENTER_FRAME, onSoundUpdate );
			
			_position = 0.0;
			
			_playFrom = _loopAt;
			
			if( _loop == true )
				this.play();
		}
		
		private function onSoundUpdate( e:Event ):void
		{
			_position = (_channel.position/_sound.length);
			//trace( (_channel.position/_sound.length), _channel.leftPeak, _channel.rightPeak );
		}
	}
}