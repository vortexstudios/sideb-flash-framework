﻿package com.vortexgamestudios.framework.soundmanager
{
	import com.greensock.TweenLite;
	import flash.media.Sound;

	public class SoundManager
	{
		private var _soundList:Vector.<SoundClip> = new Vector.<SoundClip>();
		private var _mute:Boolean = false;
		private var _volume:Number = 1.0;

		public function SoundManager()
		{
			
		}
		
		public function set volume( val:Number ):void
		{
			if( val < 0.0 )
				val = 0.0;
			else if( val > 1.0 )
				val = 1.0;
			_volume = val;
			
			for( var q:int = 0; q < _soundList.length; q++ )
			{
				_soundList[q].volume = val;
			}
		}
		
		public function get volume():Number
		{
			return _volume;
		}
		
		public function addSound( name:String, sound:Class, loop:Boolean = false, loopAt:Number = 0.0 ):void
		{
			var tSound:SoundClip = new SoundClip( name, (new sound as Sound), loop, loopAt );
			_soundList.push( tSound );
		}
		
		public function play( name:String ):void
		{
			for( var q:int = 0; q < _soundList.length; q++ )
			{
				if( _soundList[q].name == name )
				{
					_soundList[q].play();
				}
			}
		}
		
		public function stop( name:String ):void
		{
			for( var q:int = 0; q < _soundList.length; q++ )
			{
				if( name == _soundList[q].name )
				{
					_soundList[q].channel.stop();
				}
			}
		}
		
		public function stopAll():void
		{
			for( var q:int = 0; q < _soundList.length; q++ )
			{
				_soundList[q].channel.stop();
			}
		}
		
		public function togleMute():void
		{
			_mute = !_mute;
			
			if( _mute == true )
			{
				TweenLite.to( this, 1, {volume: 0.0} );
			}
			else
			{
				TweenLite.to( this, 1, {volume: 1.0} );
			}
		}
	}
}