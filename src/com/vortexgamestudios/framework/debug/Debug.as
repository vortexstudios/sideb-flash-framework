﻿package com.vortexgamestudios.framework.debug
{
	import flash.system.Capabilities;

	public class Debug
	{	
		private static var _output:String = "";
		private static var _stack:String;
		
		public function Debug()
		{

		}
		
		public static function Log( ...param )
		{
			if (Capabilities.isDebugger)
			{
				_stack = new Error().getStackTrace();
				
				_output = "";
				
				if ( getLineNumber() )
					_output += "(Line " + getLineNumber () + ") ";
					
				_output += getObject() + " - ";
					
				while( param.length > 0 )
				{
					_output += param[0] + " ";
					param.shift();
				}
				
				trace( _output );
			}
			else
			{
				//trace ("DebugTracer requires the debug player.");
			}
		}
		
		private static function getObject ():String
		{
			var errorParts:Array;
			var objectName:String;
			
			errorParts = String( _stack.split("\t")[2] ).split ("::");
			objectName = String( errorParts[1] ).split ("()")[0];
			
			return objectName;
		}
		
		// PARA RETORNAR NÚMERO DA LINHA A FLAG "Permit Debugging" DEVE ESTAR ATIVADA!!
		private static function getLineNumber ():Number
		{
			var errorParts:Array;
			var lineNumber:Number;
			
			errorParts = String( _stack.split("\t")[2] ).split (":");
			lineNumber = Number( String( errorParts[errorParts.length - 1] ).split ("]")[0]);
			
			return lineNumber;
		}
	}
}