package com.vortexgamestudios.framework {
	import flash.geom.Point;
	
	public class Vector2 {
		
		private var _x:Number=0;
		private var _y:Number=0;
		
		private var _degToRad:Number = 0.01745329251994329576923690768489;
		
		public function Vector2( x = 0, y = 0 ) {
			// constructor code
			_x = x;
			_y = y;
		}
		
		public function get magnitude():Number{ 
			return Math.sqrt( _x * _x + _y * _y );
		}
		
		public function get angle():Number{ 
			var radAngle:Number = Math.atan2( _x , -_y )
			var radians:Number;
			var degrees:Number;
			
			if (radAngle > 0)
				radians = radAngle;
			else 
				radians = 2*Math.PI + radAngle;
			
			degrees = radians * 360 / (2*Math.PI);
			
			return degrees;
		}
		
		public function rotate( angle:Number ):Vector2{
			var rads:Number = angle*_degToRad;
			var xComponent:Number = _x * Math.cos( rads ) - _y * Math.sin( rads );
			var yComponent:Number = _x * Math.sin( rads ) + _y * Math.cos( rads );
			
			var resultantVector:Vector2 = new Vector2( xComponent , yComponent );
			
			return resultantVector;
		}
		
		public function get x():Number{
			return _x;
		}
		public function get y():Number{
			return _y;
		}
		
		public function get normalized():Vector2{
			var resultantVector:Vector2 = new Vector2(0,0);
			if( this.magnitude != 0)
				resultantVector = Vector2.div( this , this.magnitude );
			
			return resultantVector;
		}
		
		static public function mult( vector:Vector2 , scalar:Number ):Vector2{
			var resultantVector:Vector2 = new Vector2( vector.x * scalar , vector.y * scalar )  ;
			return resultantVector;
		}
		
		static public function div( vector:Vector2 , scalar:Number ):Vector2{
			var resultantVector:Vector2 = new Vector2( vector.x / scalar , vector.y / scalar )  ;
			return resultantVector;
		}
		
		static public function sum( ... args ):Vector2 {
			var k = 0;
			var xComponent:Number = 0;
			var yComponent:Number = 0;
			
			for( k = 0 ; k < args.length ; k++ ){
				xComponent += args[k].x;
				yComponent += args[k].y;
			}
			
			var resultantVector:Vector2 = new Vector2( xComponent , yComponent );
			return resultantVector;
		}
		
		public function get negative():Vector2 {
			return new Vector2( -_x , -_y );
		}
		
		public function get zero():Vector2{
			return new Vector2();
		}
		
		function toString():String {
			return "[Vector2: ("+_x+","+_y+")]";
		}
		
		function toPoint():Point {
			return new Point( _x , _y );
		}	
	}	
}