package com.vortexgamestudios.framework {
	import appkit.responders.NResponder;
	import com.vortexgamestudios.framework.scenemanager.SceneManager;
	import com.vortexgamestudios.framework.soundmanager.SoundManager;
	import com.vortexgamestudios.framework.TimeManager.TimeManager;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;

	/**
	 * ...
	 * @author ...
	 */
	public class SideB {
		private static var _stage:Stage;
		
		private static var _sceneManager:SceneManager = new SceneManager();
		private static var _soundManager:SoundManager = new SoundManager();
		private static var _timeManager:TimeManager = new TimeManager();
		
		public static function setStage( val:Stage ):void {
			_stage = val;
			
			if( _stage != null ) {
				NResponder.addNative( _stage, Event.ENTER_FRAME, update );
				
				NResponder.addNative( _stage, KeyboardEvent.KEY_DOWN, update );
				NResponder.addNative( _stage, KeyboardEvent.KEY_UP, update );
				NResponder.addNative( _stage, MouseEvent.CLICK, update );
				NResponder.addNative( _stage, Event.ENTER_FRAME, update );
			}
		}
		
		public static function getstage():Stage {
			return _stage;
		}
		
		public static function get Scenes():SceneManager {
			if ( _sceneManager == null )
				_sceneManager = new SceneManager();
				
			return _sceneManager;
		}
		
		public static function get Sounds():SoundManager {
			if ( _soundManager == null )
				_soundManager = new SoundManager();
				
			return _soundManager;
		}
		
		private static function update( e:Event ) {
			NResponder.dispatch( "SIDEB_UPDATE" );
		}
	}
}