﻿package com.vortexgamestudios.framework.scenemanager.transitions
{	
	import com.vortexgamestudios.framework.scenemanager.SceneClip;
	import com.greensock.TweenLite;
	import com.vortexgamestudios.framework.scenemanager.transitions.filters.*

	public class Transitions
	{
		// FADE
		public static function FADE( object:SceneClip, value:Number ):void
		{
			object.alpha = value;
		}
		
		// PIXELATE
		public static function PIXELATE( object:SceneClip, value:Number ):void
		{
			if( value >= 1.0 )
			{
				object.filters = [];
				return;
			}
			var pixelate:PixelateBlender = new PixelateBlender();
			pixelate.value = Math.ceil((1-value)*32.0);
			object.filters = [pixelate];
		}

		// LEFT TRANSITION
		public static function SLIDE_LEFT( object:SceneClip, value:Number ):void
		{
			object.x = -object.width * (1-value);
		}

		// RIGHT TRANSITION
		public static function SLIDE_RIGHT( object:SceneClip, value:Number ):void
		{
			object.x = object.width * (1-value);
		}

		// UP TRANSITION
		public static function SLIDE_UP( object:SceneClip, value:Number ):void
		{
			object.y = -object.height * (1-value);
		}
		
		// DOWN TRANSITION
		public static function SLIDE_DOWN( object:SceneClip, value:Number ):void
		{
			object.y = object.height * (1-value);
		}
	}
}