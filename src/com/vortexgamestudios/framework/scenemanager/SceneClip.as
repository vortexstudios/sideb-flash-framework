﻿package com.vortexgamestudios.framework.scenemanager {	
	import flash.display.MovieClip;
	import com.vortexgamestudios.framework.scenemanager.transitions.Transitions;
	
	public class SceneClip extends MovieClip {
		// Static variables (ALWAYS RESET)
		/*
		public var childScene:SceneClip = null;
		public var parentScene:SceneClip = null;
		*/
		
		// Private variables
		private var _address:String = "";
		public function get address():String { return _address; }
		
		private var _parameters:Object = null;
		public function get parameters():Object { return _parameters; }
		
		private var _transition:SceneTransition = null;
		public function get transition():SceneTransition { return _transition; }
		
		// Public variables
		
		// Public functions
		public function SceneClip( address:String, parameters:Object ) {
			this._address = address;
			this._parameters = parameters;
			this._transition = new SceneTransition( this );
			
			//this._transition.open = [Transitions.FADE, Transitions.PIXELATE ];
			this._transition.openDelay = 0.0;
			this._transition.openDuration = 0.5;
			
			//this._transition.close = [Transitions.FADE, Transitions.PIXELATE];
			this._transition.closeDelay = 0.0;
			this._transition.closeDuration = 0.5;
		}
		
		// Private functions
		
		// Callbacks to Override
		// Override this function to execute the callback when this scene open
		public function onOpen():void{ }
		
		// Override this function to execute the callback when this scene close
		public function onClose():void { }
		
		// Override this function to execute a action every frame
		public function onUpdate():void { }
	}
}