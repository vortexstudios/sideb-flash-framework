﻿package com.vortexgamestudios.framework.scenemanager {	
	import appkit.responders.NResponder;
	import flash.display.MovieClip;
	import flash.utils.Dictionary;
	
	public class SceneManager {
		private var _sceneList:Dictionary = new Dictionary();
        private var _lastSceneAddress:String = "";
        private var _currentSceneAddress:String = "";

		public function SceneManager() {
			NResponder.add( "SIDEB_UPDATE", this.updateScenes );
		}
		
		public function listScene():void {
			for each (var tScene:Object in _sceneList) {
				trace('"' + tScene.Address + '" > {' + tScene.Parent + "; " + tScene.Scene + "}" );
			}
		}
		
		public function addScene( parent:MovieClip, address:String, scene:Class ):void {
			var tScene:Object = new Object();
			tScene.Address = address;
			tScene.Scene = scene;
			tScene.Parent = (parent as MovieClip);
			
			_sceneList[address] = tScene;
		}

		public function toScene( address:String, parameters:Object = null ):void {
			_currentSceneAddress = address;
			trace( "--->", _currentSceneAddress, _lastSceneAddress )

			var tScene:Object = _sceneList[address];					// Scene
			var tParent:MovieClip = (tScene.Parent as MovieClip);		// Parent
			//tObject:SceneClip = new (tScene.Scene as Class);			// Conteúdo
			
			var tAddress:Array = address.split("/");					// Endereço atual
			var tLastArray:Array = _lastSceneAddress.split("/");		// Endereço antigo
			var tContent:Array;

			var tOpenAddress:String = "";
			var tCloseAddress:String = "";

			//this._currentSceneAddress = address;

			closeScenes( tAddress, tLastArray );
			
			var q:int = 0;
			var l:int = tAddress.length;
			for( q = 0; q < l; q++ ) {
				tOpenAddress += tAddress[q];
				trace( tOpenAddress );
				if ( tOpenAddress == address )
					this.openScene( tParent, tOpenAddress, parameters );
				else
					this.openScene( tParent, tOpenAddress, null );
				tOpenAddress += "/";
			}
			
			_lastSceneAddress = address;
		}
		
		private function openScene( parent:MovieClip, address:String, parameters:Object = null ):void {
			var tAddress:String = "scene_" + address.split("/").join("_");

			if ( parent.getChildByName( tAddress ) != null )
				return;

			var tScene:Object = _sceneList[address];
			var tParent:MovieClip = (tScene.Parent as MovieClip);
			var tObject:SceneClip = new (tScene.Scene as Class)( address, parameters );
			
			tObject.name = tAddress;
			parent.addChild( tObject );
			(tObject as SceneClip).transition.doOpenTransition();
		}
		
		private function closeScene( address:String ):void {
			var tAddress:String = "scene_" + address.split("/").join("_");
			
			var tScene:Object = _sceneList[address];
			var tParent:MovieClip = (tScene.Parent as MovieClip);
			var tObject:SceneClip = tParent.getChildByName( tAddress ) as SceneClip;
			
			tObject.transition.doCloseTransition();
		}
		
		private function closeScenes( openArray:Array, closeArray:Array ):void {
			var q:int = 0;
			var openAddress:String = "";
			var closeAddress:String = "";
			
			if( openArray[0] == "" ||
			   	closeArray[0] == "" ||
				openArray[0] == undefined ||
			   	closeArray[0] == undefined )
				return;
				
			if( openArray.length > closeArray.length ) {
				//trace( "=====> OPENORDER" );
			} else {
				//trace( "=====> CLOSEORDER" );
				for( q = 0; q < closeArray.length; q++ ) {
					openAddress += openArray[q];
					closeAddress += closeArray[q];
					
					//trace( openAddress, closeAddress );
					//trace( "FECHAR ==>", closeAddress, q, closeArray.length );	
					if( openAddress != closeAddress ) {
						this.closeScene( closeAddress );
						//q = closeArray.length;
						//break;
					}
					
					openAddress += "/";
					closeAddress += "/";
				}
			}
		}
		
		private function updateScenes(): void {
			for each (var tScene:Object in _sceneList) {
				var tAddress:String = "scene_" + tScene.Address.split("/").join("_");
				var tParent:MovieClip = (tScene.Parent as MovieClip);
				var tObject:SceneClip = tParent.getChildByName( tAddress ) as SceneClip;
				
				if ( tObject ) {
					tObject.onUpdate();
				}
			}
		}
	}
}