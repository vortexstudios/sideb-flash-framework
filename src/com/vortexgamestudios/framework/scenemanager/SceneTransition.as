﻿package com.vortexgamestudios.framework.scenemanager 
{	
	import com.greensock.TweenLite;
	
	public class SceneTransition
	{
		// Private variables
		private var _scene:SceneClip = null;
		
		private var _openTransition:Boolean = true;
		private var _value:Number = 1.0;

		public function set value( val:Number ):void
		{
			var q:int = 0;
			var l:int = 0;
			if( _openTransition == true )
			{
				l = this.open.length;
				for( q = 0; q < l; q++ )
				{
					this.open[q].call( null, 
									   this._scene, 
									   val );
				}
			}
			else
			{
				l = this.close.length;
				for( q = 0; q < l; q++ )
				{
					this.close[q].call( null, 
									    this._scene, 
									    val );
				}				
			}
			
			_value = val;
		}
		
		public function get value():Number { return _value; }

		// Public variables
		public var open:Array = null;
		public var openDuration:Number = 1.0;
		public var openDelay:Number = 0.0;
		
		public var close:Array = null;
		public var closeDuration:Number = 1.0;
		public var closeDelay:Number = 0.0;
		
		// Public functions
		public function SceneTransition( scene:SceneClip )
		{
			this.open = null; 
			this.close = null;
			
			this._scene = scene;
		}
		
		public function doOpenTransition():void
		{
			if( this.open == null || this.open.length == 0 )
				this._scene.onOpen();
			else
			{
				_openTransition = true;
				doTransition();
				
				/*
				var q:int = 0;
				for( q = 0; q < this.open; q++ )
				{
					this.open[q].call( null, 
									   this._scene, 
									   this.openDelay, 
									   this.openDuration, 
									   this._scene.onOpen );
				}
				*/
			}
		}
		
		public function doCloseTransition():void
		{
			if ( this.close == null || this.close.length == 0 )
			{
				this._scene.onClose();
				this._scene.parent.removeChild( this._scene );
			}
			else
			{
				_openTransition = false;
				doTransition();
				
				/*
				this.open.call( null, 
							    this._scene, 
								this.closeDelay, 
								this.closeDuration, 
								this._scene.onClose );
				*/
			}
		}
		
		// Private functions
		private function doTransition()
		{
			if( _openTransition == true )
				TweenLite.from( this, this.openDuration, { value: 0.0, delay: this.openDelay, onComplete: this._scene.onOpen } );
			else
				TweenLite.to( this, this.openDuration, { value: 0.0, delay: this.closeDelay, onComplete: this.destroyScene } );
		}
		
		// TEMPORARIAMENTE AQUI!! ISSO É TRABALHO DO SCENEMANAGER, E NÃO DO SCENETRANSITION
		private function destroyScene():void
		{
			this._scene.onClose.call();
			this._scene.parent.removeChild(this._scene);
		}
	}
}