package com.vortexgamestudios.framework.TimeManager {
	import appkit.responders.NResponder;
	import flash.utils.getTimer;
	
	/**
	 * ...
	 * @author ...
	 */
	public class TimeManager {
		private var _lastTime:Number = 0;
		private var _totalTime:Number = 0;
		private var _deltaTime:Number = 0;
		
		public function TimeManager() {
			NResponder.add( "SIDEB_UPDATE", this.updateTime );
			_lastTime = getTimer();
		}
		
		public function get Time():Number {
			return _totalTime;
		}
		
		public function get DeltaTime():Number {
			return _deltaTime;
		}
		
		private function updateTime():void {
			// get the current time
			var t:int = getTimer();
			
			// calculate the delta time
			_deltaTime = ( t - _lastTime ) * 0.001;
			_lastTime = t;
			
			// calculate the total time
			_totalTime += _deltaTime;
		}
	}
}