﻿package {	
	import app.scenes.*;
	import com.vortexgamestudios.framework.SideB;
	import flash.display.MovieClip;
	
	public class Main extends MovieClip {
		public function Main() {	
			// Set the stage, we will use this guy a lot!!
			SideB.setStage( stage );
			
			// The addSound was made to load musics with intro and loop, use the loopAt to set where the loop starts.
			SideB.Sounds.addSound( "marioworld", marioworld, true, 1800.0 );
			// Now let's play the music
			SideB.Sounds.play("marioworld");
			
			SideB.Scenes.addScene( this, "title", SceneTitle );
			SideB.Scenes.addScene( this, "title/circle", SceneCircle );
			SideB.Scenes.toScene( "title/circle", { firstParam: "text", secondParam: 12345 } );	
		}
	}
}
