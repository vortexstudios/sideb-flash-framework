﻿package app.scenes {
	
	import com.vortexgamestudios.framework.debug.Debug;
	import com.vortexgamestudios.framework.scenemanager.SceneClip;
	import com.vortexgamestudios.framework.scenemanager.transitions.Transitions;
	
	public class SceneTitle extends SceneClip {
		public function SceneTitle( address:String, parameters:Object ) {
			super( address, parameters );
			
			// Set transition stuff here
			this.transition.open = [Transitions.FADE, Transitions.PIXELATE];
			this.transition.openDelay = 5.0;
			this.transition.openDuration = 2.0;
		}
		
		override public function onOpen():void {
			Debug.Log("Scene Title Done");
		}
	}
}
