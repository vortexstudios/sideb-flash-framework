﻿package app.scenes {	
	import com.vortexgamestudios.framework.debug.Debug;
	import com.vortexgamestudios.framework.scenemanager.SceneClip;
	import com.vortexgamestudios.framework.scenemanager.transitions.Transitions;
	import com.vortexgamestudios.framework.SideB;
	
	public class SceneCircle extends SceneClip {
		public function SceneCircle( address:String, parameters:Object ) {
			super( address, parameters );
			
			// Set transition stuff here
			// Transition effects, yes, you can mix then.
			this.transition.open = [Transitions.SLIDE_DOWN, Transitions.FADE];
			// Delay, wait 6 seconds to start to open.
			this.transition.openDelay = 6.0;
			// And the duration, the transition will take 2 secs.
			this.transition.openDuration = 2.0;
			
			this.transition.close = [Transitions.SLIDE_UP, Transitions.FADE];
			this.transition.closeDelay = 1.0;
			this.transition.closeDuration = 3.0;
		}
		
		override public function onOpen():void {
			Debug.Log("SceneCircle Done", this.parameters.param );
			
			// Let's close this scene
			SideB.Scenes.toScene("title");
		}
		
		override public function onClose():void {
			Debug.Log("Closed");
		}
		
		override public function onUpdate():void {
			// This function is called every frame, you didn't need to create a ENTER_FRAME event for this.
			//trace( "onUpdate callback" );
		}	
	}
}